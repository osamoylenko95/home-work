package task1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class task1 {

	public static void main(String[] args) {

		List<String> l1 = new ArrayList<String>();
		
		Collections.addAll(l1, "Element1", "Element1", "Element3", "Element4", "Element5", "Element6", "Element1");
		System.out.println("Initial list = " +l1);
		Collections.replaceAll(l1.subList(0, (int) Math.round(((double) l1.size() / 2))), "Element1", "Replaced Element");
		System.out.println("List With Replaced Element = " +l1);
	}
	
}