package task2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class test {
	public static void main(String[] args) {
	int[] arrayOfNumbers = new int[10];

 	Arrays.fill(arrayOfNumbers, 10);

	System.out.println(Arrays.toString(arrayOfNumbers));

	System.out.println(
		Collections.lastIndexOfSubList(
			Arrays.asList(10, 100, 90, 60, 90, -80, 100, 90, 100, 90, 0),
			Arrays.asList(100, 90)
		)
	);

	List<String> words = new ArrayList<>();
	
	Collections.addAll(words, 
			"gsldjfkhsdf", "LITS", "Automation",
			"asdbaksjhd", "LITS", "Automation", "asdasdlhgkajhshdk",
			"gakhsjdaksd", "LITS", "Automation");

	System.out.println(words);
	
	for (int i = 0 ; i < words.size(); i++) {
		for (int j = i; j < words.size(); j++) {
			if (i!=j && words.get(i).equals(words.get(j))) {
				words.remove(j); //i
			}				
		}
	}
	
	System.out.println(words);
	
	Set<String> wordsFromList = new HashSet<>(words);
	System.out.println(wordsFromList);
	
	List<String> randomList = new ArrayList<String>();
	Collections.addAll(randomList, "abc", "cba", "ddd", "cba", "cba", "abc", "rrr", "ddd", "ahskd"); //9		
	Collections.replaceAll(randomList.subList(0, (int) Math.round(((double) randomList.size() / 2))), "cba", "kkk");

	Integer [] masyv = {10, 10, 123, 1231, 3 , 123, 88, 10, 3, 1231};

	List<Integer> list = Arrays.asList(masyv);
	Collections.reverse(list);
	
	Set<Integer> setWithoutDuplicates = new HashSet<Integer>(list);
	System.out.println(setWithoutDuplicates);

	Integer [] masyv2 = new Integer [setWithoutDuplicates.size()];
	System.out.println(masyv2.length);
	masyv2 = setWithoutDuplicates.toArray(masyv2);
	
	System.out.println(Arrays.toString(masyv2));

}
}
