package task2;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class task2 {
	
		public static void main(String[] args) {
				   
		    int[] array = {1,1,2,2,2,3,3,4,5,6,8};
		    System.out.println("Initial array: " + Arrays.toString(array) );
		    
		    int [] reverseArray = new int [array.length];
		    		    
		    for (int i = array.length - 1; i >= 0; i--) {
		        reverseArray[reverseArray.length - 1 - i] = array[i];
		    }
		    System.out.println("Reversed array: " + Arrays.toString(reverseArray) );
		   
		    Set<Integer> set = new LinkedHashSet<Integer>();
		    
		    for (int num : array) {
		        set.add(num);
		    }

		    System.out.println("Array after duplicates deleted" +set);
		}
	
}
