package task3;

import java.util.Arrays;

public class task3 {
	
		public static void main(String[] args) {
					   
		    int[] array = {3,4,5,6,1,10,15,17,4,3,1};
		    System.out.println("Initial array " + Arrays.toString(array));
		    int indexNeedValue = 0;
		    int sum = 0;
		    int avarage = 0;
		   // Arrays.sort(array);
		    System.out.println("Min element = " + getMinValue(array));
		    for (int i = array.length - 1; i > 0; i--) {
		    	sum = sum + array[i];
		    }
		    for (int i = array.length - 1; i > 0; i--) {
		    	if (array[i] == getMinValue(array)) {
		    		indexNeedValue = i;
		    		break;
		    	}
		    }
		    
		    avarage = Math.round(sum / array.length);
		    System.out.println("Index of last minimum value = " + indexNeedValue);
		    System.out.println("Sum of elements in array = " + sum);
		    System.out.println( "Avarage =" + avarage);
		    array[indexNeedValue] = avarage;
		    System.out.println("Array where min element was replaced by avarage = " + Arrays.toString(array));
		}
		public static int getMinValue(int[] numbers){
			int minValue = numbers[0];
			for(int i=1;i<numbers.length;i++){
				if(numbers[i] < minValue){
					minValue = numbers[i];
				}
			}
			return minValue;
		}
}
